#!/bin/sh
#
# Set variables
#   * PATkn :
#       Personnal Access Token
#   * LAST_UPSTREAM_COMMIT_BUILT :
#       updated on successful build
#   * MPD_BRANCH :
#       Which branch to build, "master", "v0.21.x"
#
set -e
LAST_CMT_VAR=LAST_BUILD_COMMIT_$(echo $MPD_BRANCH| tr . _)_${TARGET_DEBIAN_RELEASE}

set_variable () {
  curl --request PUT --header "PRIVATE-TOKEN: ${PATkn}" \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/variables/$1" \
    --form "value=$2" --silent
}
cancel_pipeline () {
  # curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipelines/46/cancel"
  # /projects/:id/pipelines/:pipeline_id/cancel
  curl --request POST --header "PRIVATE-TOKEN: ${PATkn}" \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/cancel"
  #wget --method=POST --header="PRIVATE-TOKEN: ${PATkn}"  \
  #  "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/cancel"
}
set_watch_file () {
  cat <<EOF > ./debian/watch
  version=4
  opts="mode=git, pretty=${MPD_VERSION%%~git}~git%cd.%h, pgpmode=none," \
  https://github.com/MusicPlayerDaemon/MPD.git \
  heads/${MPD_BRANCH} debian
EOF
}
set_upstream_remote () {
  test -d .git/refs/remotes/upstream/ || git remote add upstream https://github.com/MusicPlayerDaemon/MPD.git
  # TODO: use "fetch" instead of "remote update", it allows limiting fetching
  git fetch --quiet --depth=10 upstream
}
last_upstream_commit_built () {
  eval echo \$$LAST_CMT_VAR
}
last_upstream_commit () {
  set_upstream_remote > /dev/null
  LAST_UPSTREAM_COMMIT=$(git rev-parse --short=7 upstream/${MPD_BRANCH})
  echo "$LAST_UPSTREAM_COMMIT"
}
changes_upstream () {
  set_upstream_remote
  # Get current upstream version
  MPD_VERSION=$(git show upstream/${MPD_BRANCH}:meson.build | grep -Po "(?<=version: ')[[:alnum:].~]*(?=')")
  echo → Found this version git: upstream/${MPD_BRANCH}:meson.build : ${MPD_VERSION}
  if ! git branch -r | grep -q "upstream/${MPD_BRANCH:-X}"; then
    echo Failed to find \"upstream/${MPD_BRANCH:-Undefined MPD_BRANCH}\" in remote
    exit 1
  fi
  LAST_UPSTREAM_COMMIT=$(last_upstream_commit)
  LAST_UPSTREAM_COMMIT_BUILT=$(last_upstream_commit_built)
  if [ "$LAST_UPSTREAM_COMMIT" = "$LAST_UPSTREAM_COMMIT_BUILT" ];then
    echo Already built "${MPD_BRANCH}/${LAST_UPSTREAM_COMMIT}", canceling pipeline
    sleep 5
    cancel_pipeline
    exit 0
  else
    echo last commits: "${MPD_BRANCH}/${LAST_UPSTREAM_COMMIT_BUILT}" → "${MPD_BRANCH}/${LAST_UPSTREAM_COMMIT}"
  fi
}

fetch_upstream () {
  echo " -> Merging upstream changes"
  # Configure Git
  test -n"${GITLAB_USER_NAME}"  && git config --global user.name "${GITLAB_USER_NAME}"
  test -n"${GITLAB_USER_EMAIL}" && git config --global user.email "${GITLAB_USER_EMAIL}"

  export MPD_VERSION=$(curl https://raw.githubusercontent.com/MusicPlayerDaemon/MPD/${MPD_BRANCH}/meson.build \
    | grep -Po "(?<=version: ')[[:alnum:].~]*(?=')")
  # Setting watch file to fetch latest changes
  set_watch_file
  git commit -qa -m'Update watch file to fetch from git' -m'Gbp-Dch: Ignore'

  gbp import-orig --no-pristine-tar --uscan --no-interactive
  LAST_UPSTREAM_COMMIT=$(last_upstream_commit)
  PKG_VERSION=$(git log -1 --pretty=format:%s upstream | grep -Po '(?<=New upstream version ).*')
  echo "    tagging ${PKG_VERSION} debian/changelog"
  gbp dch --spawn-editor=never -aR --git-author --new-version ${PKG_VERSION}-1
  git commit -q debian/changelog -m"Update changelog (${PKG_VERSION}})" -m"Gbp-Dch: Ignore"
}

install_build_deps () {
  echo " -> Starting installing the build-deps"

  /usr/bin/mk-build-deps debian/control

  FILENAME=$(echo *-build-deps*.deb)
  PKGNAME=$(dpkg-deb -f ${FILENAME} Package)
  dpkg --force-depends --force-conflicts -i $FILENAME
  aptitude -y --without-recommends \
    -o "Dpkg::Options::=--force-confold" \
    -o "APT::Install-Recommends=false" \
    -o "Aptitude::ProblemResolver::StepScore=100" \
    -o "Aptitude::ProblemResolver::SolutionCost=safety, priority, non-default-versions" \
    -o "Aptitude::ProblemResolver::Hints::KeepDummy=reject $PKGNAME :UNINST" \
    -o "Aptitude::ProblemResolver::Keep-All-Level=55000" \
    -o "Aptitude::ProblemResolver::Remove-Essential-Level=maximum" \
    install $PKGNAME
  rm $FILENAME
  if ! dpkg -l $PKGNAME 2>/dev/null | grep -q ^ii; then
    echo "Aptitude couldn't satisfy the build dependencies"
    exit 1
  fi
  echo " -> Finished installing the build-deps"
}

build_success () {
  # Build successful, setting last build commit in project/CI
  LAST_UPSTREAM_COMMIT=$(last_upstream_commit)
  echo Settings variable $LAST_CMT_VAR to $LAST_UPSTREAM_COMMIT
  set_variable $LAST_CMT_VAR $LAST_UPSTREAM_COMMIT
}

if [ $# -eq 0 ]
then
    fetch_upstream
else
  $@
fi
# vim: fileencoding=utf8
